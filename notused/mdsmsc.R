library(Seurat)
library(clusterProfiler)
library(org.Hs.eg.db)
library(tidyverse)


msc <- readRDS("/scratch/atyrlik/YUKOMDS/map_sub.rds")

msc@meta.data <- msc@meta.data %>%
  mutate(trans = if_else(orig.ident == "Normal_BM_57M_58M", "Normal", "MDS"))
Idents(msc) <- "trans"
# submsc <- subset(msc, subset = seurat_clusters %in% c("0", "1"))
# 
# fc <- FoldChange(submsc, ident.1 = "MDS", ident.2 = "Normal", group.by = "trans") %>%
#   mutate(avg_log2FC = abs(avg_log2FC)) %>%
#   arrange(desc(avg_log2FC)) %>%
#   mutate(., rank = seq(1:nrow(.)))
# ggplot(fc, aes(x = rank, y = avg_log2FC)) + geom_point() + geom_hline(aes(yintercept = 0.01))

mdspts <- unique(msc$orig.ident)[grepl("MDS", unique(msc$orig.ident))] %>%
  append(., list(.))
kegggseout <- list()
for (i in 1:length(mdspts)){
  submsc <- subset(msc, subset = seurat_clusters %in% c("2") & orig.ident %in% c("Normal_BM_57M_58M", mdspts[i][[1]]))
  impvec <- FindMarkers(submsc, ident.1 = "MDS", ident.2 = "Normal", logfc.threshold = 0) %>%
    rownames_to_column() %>%
    mutate(entrez = mapIds(org.Hs.eg.db, .$rowname, 'ENTREZID', 'SYMBOL')[.$rowname]) %>%
    filter(!is.na(entrez)) %>%
    arrange(desc(avg_log2FC)) %>%
    dplyr::select(entrez, avg_log2FC) %>%
    deframe()
  kegggse <- GSEA(geneList=impvec, organism = "hsa")
  kegggse <- setReadable(kegggse, OrgDb = org.Hs.eg.db, keyType = "ENTREZID")
  kegggseout <- append(kegggseout, kegggse)
}


